#!/bin/bash

#    Copyright 2016, 2017, 2018 Rafal Wisniewski (wisniew99@gmail.com)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


#############################################
#############################################
##                                         ##
####           Harfix builder            ####
##                                         ##
#############################################
#############################################
##                 1.4.4                   ##
#                                           #
# Build script for Kernels.                 #
# By wisniew99 / rafciowis1999              #
#                                           #
# Script based on inplementation            #
# in Harfix3 kernel for i9300.              #
#                                           #
# Read and edit all things in tables.       #
#                                           #
##      REMEMBER TO EDIT LOCATIONS!!!      ##
#############################################
#############################################
#                                            #
# If colours looks ugly, use black terminal.  #
#                                              #
##################################################
###################  MAIN THINGS  ################
##################################################
##                                              ##
# Project name
PRONAME="Cherry"
# Version number or name
VERSION="3.0.2"
#
#               New name = new main folder                 ##
#############################################################
####################  Edit not needed  ######################
#############################################################
##                                                         ##
## Auto detect Your home folder.                           ##
HOME="$(dirname ~)/$(basename ~)"                          ##
##                                                         ##
## Maximum jobs that Your computer can do.                 ##
JOBS="$(grep -c "processor" "/proc/cpuinfo")"              ##
##                                                         ##
## Colors                                                  ##
red=$(tput setaf 1)                 # red     # Error      ##
grn=$(tput setaf 2)                 # green   # Done       ##
ylw=$(tput setaf 11)                # yellow  # Warring    ##
blu=$(tput setaf 4)                 # blue    # path       ##
gren=$(tput setaf 118)              # green   # Name       ##
pur=$(tput setaf 201)               # purple  # Name       ##
txtbld=$(tput bold)                 # Bold    # Info       ##
bldred=${txtbld}$(tput setaf 1)     # Red     # Error desc ##
bldblu=${txtbld}$(tput setaf 4)     # blue    # Info       ##
txtrst=$(tput sgr0)                 # Reset                ##
##                                                         ##
#############################################################
########################  CONFIGS  ##########################
#############################################################
##                                                          ##
##                                                            ##
CONFIG=oneplus5_defconfig                      # config name    ##
##                                                                ##
##                                                                 ##
#####################################################################
###########################  EDIT THIS!  ############################
#####################################################################
##                                                                 ##
##                                                                 ##
ARCH=arm64                                       # arch of device  ##
SUBARCH=arm64                                  # subarch of device ##
USER=Wisniew                                     # Name of builder ##
HOST=Harfix-machine                              # name of machine ##
TC_FLAGS_KERNEL="-Wno-maybe-uninitialized"      # Flags for kernel ##
TC_FLAGS_MODULE="-Wno-maybe-uninitialized"     # Flags for modules ##
OUTPUT="Image.gz-dtb"                  # output compile image name ##
ZIPIMAGE="Image.gz-dtb"                       # image name for zip ##
SILENCE=1                                        # silance compile ##
LINE="9"                        # line in conifg with localversion ##
TCDIR=$HOME/TC                                     # Toolchain dir ##
TCNAME="google-ndk"                               # Toolchain name ##
TCEND="bin/aarch64-linux-android-"         # End of toolchain name ##
TCLIB="lib/"                                    # lib folder in TC ##
##                                                                 ##
##                                                                 ##
##                      ##  TC example:  ##                        ##
##                     $TCDIR/$TCNAME/$TCEND                       ##
##                                                                 ##
#####################################################################
###########################  BUILD PATHS  ###########################
#####################################################################
##                                                                 ##
PATHZIMAGE="kernels/custom"                          # zImage path ##
PATHMODULES="ramdisk/modules"                   # modules path for ##
##                                                                 ##
##                                                                 ##
##                      ##  Path example:  ##                      ##
##                  $PRONAME/ZIP_FILES/PATHZIMAGE                  ##
##                  $PRONAME/ZIP_FILES/PATHMODULES                 ##
##                                                                 ##
#####################################################################
#####################################################################

################
# Welcome info #
################

echo -e '\0033\0143'
echo "${gren} Welcome to Harfix builder!${txtrst}"
echo "${gren} By${txtrst} ${pur}wisniew99 / rafciowis1999 ${txtrst} "
echo "${gren} Script is licenced under GNU GPL v2${txtrst}"
echo ""
echo ""
echo "${gren} Status:${txtrst}"

#######################
## Testing Toolchain ##
#######################

if [ ! -e $TCDIR/$TCNAME/$TCENDgcc ]
then
    echo "${txtbld} Toolchain - ${txtrst}${red}NOT OK ${txtrst}"
    echo "${bldred} Toolchain is NOT set correctly. ${txtrst}"
    echo "${txtbld} Change dir or name in script ${txtrst}"
    echo "${txtbld} to correctly set up toolchain. ${txtrst}"
    echo ""
    exit 1
fi

echo "${txtbld} Toolchain - ${txtrst}${grn}OK ${txtrst}"

#####################
# Detecting folders #
#####################

if [ ! -e "$PRONAME" -o ! -e "$PRONAME/Releases" -o ! -e "$PRONAME/STATIC_MODULES" -o ! -e "$PRONAME/ZIP_FILES" ];
then
    echo "${txtbld} Folders - ${txtrst}${red}NOT OK ${txtrst}"
    echo "${bldred} Missing folders detected ${txtrst}"
    echo ""
    echo ""
    echo ""
    echo "${txtbld} Do You want to create folders? (y/n)${txtrst}"
    echo ""
    read  -n 1 -p " Input Selection:" menuinput
    if [ "$menuinput" = "y" ];
    then
        mkdir $PRONAME
        mkdir $PRONAME/Releases
        mkdir $PRONAME/ZIP_FILES
        mkdir $PRONAME/STATIC_MODULES
        echo -e '\0033\0143'
        echo ""
        echo "${grn} Created all neccesary folders. ${txtrst}"
        echo ""
        echo "${txtbld} Put Your installer in ZIP_FILES, ${txtrst}"
        echo "${txtbld} precompiled modules to STATIC_MODULES ${txtrst}"
        echo "${txtbld} and restart script. ${txtrst}"
        echo ""
        exit 0
    elif [ "$menuinput" = "Y" ];
    then
        mkdir $PRONAME
        mkdir $PRONAME/Releases
        mkdir $PRONAME/ZIP_FILES
        mkdir $PRONAME/STATIC_MODULES
        echo -e '\0033\0143'
        echo ""
        echo "${grn} Created all neccesary folders. ${txtrst}"
        echo ""
        echo "${txtbld} Put Your installer in ZIP_FILES, ${txtrst}"
        echo "${txtbld} precompiled modules to STATIC_MODULES ${txtrst}"
        echo "${txtbld} and restart script. ${txtrst}"
        echo ""
        exit 0
    elif [ "$menuinput" = "n" ];
    then
        echo -e '\0033\0143'
        exit 0
    elif [ "$menuinput" = "N" ];
    then
        echo -e '\0033\0143'
        exit 0
    else
        echo ""
        echo ""
        echo "${red} ERROR! ${txtrst}"
        echo "${bldred} You have entered an invalid selection!${txtrst}"
        echo "${bldred} Please restart script!${txtrst}"
        echo ""
        exit 1
    fi
fi

############################################
## Testing config & changing localversion ##
############################################

if [ ! -e arch/$ARCH/configs/$CONFIG ]
then
    echo "${txtbld} Config - ${txtrst}${red}NOT OK ${txtrst}"
    echo "${bldred} Selected config does not exist. ${txtrst}"
    echo "${txtbld} Please select correct to continue. ${txtrst}"
    echo ""
    exit 1
fi

LOCALVERSION=(CONFIG_LOCALVERSION='"'"-$PRONAME-$VERSION"'"')
sed -i "$LINE s/.*/$LOCALVERSION/" arch/$ARCH/configs/$CONFIG

echo "${txtbld} Config - ${txtrst}${grn}OK ${txtrst}"

####################
# Creating folders #
####################

if [ -e "$PRONAME/work" ]
then
    rm -rf $PRONAME/work
    mkdir $PRONAME/work
    mkdir $PRONAME/work/boot
    mkdir $PRONAME/work/modules
else
    mkdir $PRONAME/work
    mkdir $PRONAME/work/boot
    mkdir $PRONAME/work/modules
fi

if [ ! -e "$PRONAME/ZIP_FILES/$PATHZIMAGE" ]
then
    mkdir -p $PRONAME/ZIP_FILES/$PATHZIMAGE
fi

if [ ! -e "$PRONAME/ZIP_FILES/$PATHMODULES" ]
then
    mkdir -p $PRONAME/ZIP_FILES/$PATHMODULES
fi

echo "${txtbld} Folders - ${txtrst}${grn}OK ${txtrst}"

############
## Cleans ##
############

if [ -e "$PRONAME/Releases/$PRONAME-$VERSION.zip" ]
then
    rm -rf $PRONAME/Releases/$PRONAME-$VERSION.zip
fi

if [ -e "arch/$ARCH/boot/$OUTPUT" ]
then
    rm -rf arch/$ARCH/boot/$OUTPUT
fi

#TODO remove modules in source

if [ -e "arch/$ARCH/boot/$OUTPUT" ]
then
    rm -rf $PRONAME/ZIP_FILES/$PATHZIMAGE/$ZIPIMAGE
fi

rm -rf $PRONAME/ZIP_FILES/$PATHMODULES/*

echo "${txtbld} Cleans - ${txtrst}${grn}OK ${txtrst}"

###############
## Main menu ##
###############

echo ""
echo ""
echo ""
echo "${txtbld} What do you want to do?${txtrst}"
echo ""
echo " Press ${bldblu}1${txtrst} to ${blu}clean${txtrst}"
echo " Press ${bldblu}2${txtrst} to ${blu}compile${txtrst}"
echo " Press ${bldblu}3${txtrst} to ${blu}clean and compile${txtrst}"
echo " Press ${bldblu}4${txtrst} to ${blu}compile and clean${txtrst}"
echo " Press ${bldblu}5${txtrst} to ${blu}compile with clean before and after${txtrst}"
echo " Press ${bldblu}6${txtrst} to ${blu}enter extra options (WIP)${txtrst}"
echo " Press ${bldblu}x${txtrst} to ${blu}exit from the script${txtrst}"
echo ""
read  -n 1 -p " Input Selection:" menuinput
if [ "$menuinput" = "1" ];
then
    CLEAN=1
    COMPILE=0
    AFTCLEAN=0
    EXTRA=0
elif [ "$menuinput" = "2" ];
then
    CLEAN=0
    COMPILE=1
    ZIP=1
    AFTCLEAN=0
    EXTRA=0
elif [ "$menuinput" = "3" ];
then
    CLEAN=1
    COMPILE=1
    ZIP=1
    AFTCLEAN=0
    EXTRA=0
elif [ "$menuinput" = "4" ];
then
    CLEAN=0
    COMPILE=1
    ZIP=1
    AFTCLEAN=1
    EXTRA=0
elif [ "$menuinput" = "5" ];
then
    CLEAN=1
    COMPILE=1
    ZIP=1
    AFTCLEAN=1
    EXTRA=0
elif [ "$menuinput" = "6" ];
then
    CLEAN=0
    COMPILE=0
    ZIP=0
    AFTCLEAN=0
    EXTRA=1
elif [ "$menuinput" = "x" ];
then
    echo -e '\0033\0143'
    exit 0
elif [ "$menuinput" = "X" ];
then
    echo -e '\0033\0143'
    exit 0
else
    echo ""
    echo ""
    echo "${red} ERROR! ${txtrst}"
    echo "${bldred} You have entered an invalid selection!${txtrst}"
    echo "${bldred} Please restart script!${txtrst}"
    echo ""
    exit 1
fi
echo -e '\0033\0143'

#############
## Exports ##
#############

export ARCH=$ARCH
export SUBARCH=$SUBARCH
export KBUILD_BUILD_USER=$USER
export KBUILD_BUILD_HOST=$HOST
export CROSS_COMPILE=$TCDIR/$TCNAME/$TCEND
export LD_LIBRARY_PATH=$TCDIR/$TCNAME/$TCLIB
STRIP=$TCDIR/$TCNAME/$TCENDstrip

##################
## Before clean ##
##################

if [ $CLEAN = 1 ]
then
    echo "${bldblu} Cleaning... ${txtrst}"
    make -j "$JOBS" clean
    make -j "$JOBS" mrproper
    echo "${grn} Cleaned. ${txtrst}"
    echo ""
    echo ""
    echo ""
fi


###############
## Compiling ##
###############

if [ $COMPILE = 1 ];
then
    echo "${txtbld} Starting compile... ${txtrst}"
    echo ""
    echo ""
    echo ""

    echo "${bldblu} Loading config... ${txtrst}"
    make $CONFIG
    echo "${grn} Done. ${txtrst}"
    echo ""

    echo "${bldblu} Compiling... ${txtrst}"
    
    if [ $SILENCE = 1 ]
    then
        make -s -j "$JOBS" CFLAGS_KERNEL="$TC_FLAGS_KERNEL" CFLAGS_MODULE="$TC_FLAGS_MODULE"
    else
        make -j "$JOBS" CFLAGS_KERNEL="$TC_FLAGS_KERNEL" CFLAGS_MODULE="$TC_FLAGS_MODULE"
    fi
    
    echo "${grn} Done. ${txtrst}"
    echo ""

    #############
    ## Zipping ##
    #############
    if [ $ZIP = 1 ];
    then
        if [ -e "arch/$ARCH/boot/$OUTPUT" ]
        then
            echo "${bldblu} Renaming Image... ${txtrst}"
            mv arch/$ARCH/boot/$OUTPUT arch/$ARCH/boot/$ZIPIMAGE 2>/dev/null
            echo "${grn} Done. ${txtrst}"
            echo ""

            echo "${bldblu} Coping zImage... ${txtrst}"
            cp arch/$ARCH/boot/$ZIPIMAGE $PRONAME/work/boot/
            echo "${grn} Done. ${txtrst}"
            echo ""

            echo "${bldblu} Coping modules... ${txtrst}"
            find -name '*.ko' -exec cp -av {} $PRONAME/work/modules/ \;
            echo "${grn} Done. ${txtrst}"
            echo ""

            echo "${bldblu} Coping files for zip... ${txtrst}"
            mv $PRONAME/work/boot/$ZIPIMAGE $PRONAME/ZIP_FILES/$PATHZIMAGE/ 2>/dev/null
            mv $PRONAME/work/modules/* $PRONAME/ZIP_FILES/$PATHMODULES/ 2>/dev/null
            cp $PRONAME/STATIC_MODULES/* $PRONAME/ZIP_FILES/$PATHMODULES/ 2>/dev/null
            echo "${grn} Done. ${txtrst}"
            echo ""

            echo "${bldblu} Deleting modules folder if empty... ${txtrst}"
            if [ -n "$(find $PRONAME/ZIP_FILES/$PATHMODULES/ -prune -empty 2>/dev/null)" ]
            then
                rm -rf $PRONAME/ZIP_FILES/$PATHMODULES
                echo "${grn} Removed empty modules folder. ${txtrst}"
            else
                echo "${blu} Modules exist, delete ignored. ${txtrst}"
            fi
            echo ""

            echo "${bldblu} Zipping... ${txtrst}"
            cd $PRONAME/ZIP_FILES
            
            if [ $SILENCE = 1 ]
            then
                zip -q -r $PRONAME.zip *
            else
                zip -r $PRONAME.zip *
            fi
            
            cd -
            echo "${grn} Done. ${txtrst}"
            echo ""

            echo "${bldblu} Moving... ${txtrst}"
            mv $PRONAME/ZIP_FILES/$PRONAME.zip $PRONAME/Releases/
            echo "${grn} Done. ${txtrst}"
            echo ""

            echo "${bldblu} Renaming... ${txtrst}"
            mv $PRONAME/Releases/$PRONAME.zip $PRONAME/Releases/$PRONAME-$VERSION.zip
            echo "${grn} Done. ${txtrst}"
            echo ""
            echo ""
            echo ""
            echo "${txtbld} Done! Saved as: $PRONAME-$VERSION.zip ${txtrst}"
        else
            echo "${red} ERROR! ${txtrst}"
            echo "${bldred} zImage NOT detected. ${txtrst}"
            echo ""
            echo ""
            echo ""

            # Cleaning
            rm -rf $PRONAME/work
            rm -rf $PRONAME/ZIP_FILES/$PATHZIMAGE/$ZIPIMAGE
            rm -rf $PRONAME/ZIP_FILES/$PATHMODULES/* #TODO test

            echo "${gren} Harfix builder completed all tasks! ${txtrst}"
            echo ""
            exit 1
        fi
    fi
else
    echo "${txtbld} Compile skipped. ${txtrst}"
fi
echo ""
echo ""
echo ""

###############
# After clean #
###############

if [ $AFTCLEAN = 1 ]
then
    echo "${bldblu} Cleaning... ${txtrst}"
    make -j "$JOBS" clean
    make -j "$JOBS" mrproper
    echo "${grn} Cleaned. ${txtrst}"
    echo ""
    echo ""
    echo ""
fi

#################
# Extra options #
#################

if [ $EXTRA = 1 ]
then
    echo -e '\0033\0143'
    echo "${gren} Extra options ${txtrst}"
    echo ""
    echo "${txtbld} Full project name: ${txtrst}${grn}$PRONAME-$VERSION${txtrst}"
    echo "${txtbld} Config in use: ${txtrst}${grn}$CONFIG${txtrst}"
    echo "${txtbld} Zipping: ${txtrst}${grn}$ZIP${txtrst}"
    echo "${txtbld} Clean after: ${txtrst}${grn}$CLEAN${txtrst}"
    echo "${txtbld} Clean before: ${txtrst}${grn}$AFTCLEAN${txtrst}"
    echo "${txtbld} Date in zip name: ${txtrst}${grn}$DATE${txtrst}"
    echo "${txtbld} Silence mode: ${txtrst}${grn}$SILENCE${txtrst}"
    echo ""
    echo ""
    echo "${txtbld} What do you want to do?${txtrst}"
    echo ""
    echo " Press ${bldblu}1${txtrst} to ${blu}change project name${txtrst}"
    echo " Press ${bldblu}2${txtrst} to ${blu}change project version${txtrst}"
    echo " Press ${bldblu}3${txtrst} to ${blu}change config (WIP)${txtrst}"
    echo " Press ${bldblu}4${txtrst} to ${blu}turn on/off zipping (WIP)${txtrst}"
    echo " Press ${bldblu}5${txtrst} to ${blu}turn on/off clean before compile (WIP)${txtrst}"
    echo " Press ${bldblu}6${txtrst} to ${blu}turn on/off clean after compile (WIP)${txtrst}"
    echo " Press ${bldblu}7${txtrst} to ${blu}turn on/off date in zip name (WIP)${txtrst}"
    echo " Press ${bldblu}8${txtrst} to ${blu}turn on/off silence compile (WIP)${txtrst}"
    echo " Press ${bldblu}b${txtrst} to ${blu}return${txtrst}"
    echo ""
    read  -n 1 -p " Input Selection:" menuinput
    if [ "$menuinput" = "1" ];
    then
        echo -e '\0033\0143'
        read  -n 20 -p " New name:" menuinput
        OLDNAME=('PRONAME="'"$PRONAME"'"')
        NEWNAME=('PRONAME="'"$menuinput"'"')
        sed -i s/$OLDNAME/$NEWNAME/ build.sh
        echo -e '\0033\0143'
        exec "./build.sh"
    elif [ "$menuinput" = "2" ];
    then
        echo -e '\0033\0143'
        read  -n 20 -p " New version:" menuinput
        OLDVER=('VERSION="'"$VERSION"'"')
        NEWVER=('VERSION="'"$menuinput"'"')
        sed -i s/$OLDVER/$NEWVER/ build.sh
        echo -e '\0033\0143'
        exec "./build.sh"
    elif [ "$menuinput" = "3" ];
    then
        #TODO
        echo -e '\0033\0143'
        exec "./build.sh"
    elif [ "$menuinput" = "4" ];
    then
        #TODO
        echo -e '\0033\0143'
        exec "./build.sh"
    elif [ "$menuinput" = "5" ];
    then
        #TODO
        echo -e '\0033\0143'
        exec "./build.sh"
    elif [ "$menuinput" = "6" ];
    then
        #TODO
        echo -e '\0033\0143'
        exec "./build.sh"
    elif [ "$menuinput" = "7" ];
    then
        #TODO
        echo -e '\0033\0143'
        exec "./build.sh"
        elif [ "$menuinput" = "8" ];
    then
        #TODO
        echo -e '\0033\0143'
        exec "./build.sh"
    elif [ "$menuinput" = "b" ];
    then
        echo -e '\0033\0143'
        exec "./build.sh"
    elif [ "$menuinput" = "B" ];
    then
        echo -e '\0033\0143'
        exec "./build.sh"
    else
        echo ""
        echo ""
        echo "${red} ERROR! ${txtrst}"
        echo "${bldred} You have entered an invalid selection!${txtrst}"
        echo "${bldred} Please restart script!${txtrst}"
        echo ""
        exit 1
    fi
fi

# Cleaning
rm -rf $PRONAME/work
rm -rf $PRONAME/ZIP_FILES/$PATHZIMAGE/$ZIPIMAGE
rm -rf $PRONAME/ZIP_FILES/$PATHMODULES/* #TODO test

echo "${gren} Harfix builder completed all tasks! ${txtrst}"
echo ""
exit 0
